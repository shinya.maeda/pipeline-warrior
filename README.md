# pipeline-warrior

```
attacker:
defender:
wizard:

stages:
  - turn1
  - turn2
  - turn3

turn1:
  stage: turn1
  script: summon attacker

turn2:
  stage: turn2
  script: summon defender

turn3:
  stage: turn3
  script: summon wizard

Pipeline: attack => 
turn1~turn2: attack
turn2~turn3: defend
turn3~end: cast spell
```